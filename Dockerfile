FROM registry.wsjeong.ocp4.local:5000/ubi8/openjdk-8:latest
    
USER root
RUN mkdir -p /logs 

# set timezone
RUN ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime

# locale
ENV LANG ko_KR.UTF-8

ADD files/HelloWorld.jar /deployments/
ADD files/startup.sh /deployments/


RUN chown root:185 /deployments -R
RUN chmod 777 /deployments -R

# Allow arbitrary
USER 185

WORKDIR /deployments
  
CMD ["tail", "-f", "/dev/null"]
#CMD ["sh", "startup.sh"]
